package com.zuitt.example;

import java.util.Scanner;
public class FactorialNumber {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        int initFactorial = 1;

        System.out.println("Input an integer whose factorial will be computed: ");

        try {
            int num = in.nextInt();

            if (num >= 1) {
                for(int i = 1; i <= num; i++) {
                    initFactorial *= i;
                }
                System.out.println("The factorial of " + num + " is: " + initFactorial);
            }

            else if (num == 0) {
                System.out.println("The factorial of " + num + " is: " + initFactorial);
            }

            else if (num < 1) {
                System.out.println("Input must be greater than 0.");
            }
        }
        catch (Exception e) {
            System.out.println("Invalid input. Please enter a valid integer.");
            e.printStackTrace();
        }

    }
}
