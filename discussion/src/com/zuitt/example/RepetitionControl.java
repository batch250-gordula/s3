package com.zuitt.example;

import java.util.ArrayList;
import java.util.HashMap;

public class RepetitionControl {
    public static void main(String[] args) {

        //[SECTION] Loops

        // 1. while Loop
        // allows for repetitive use of code, similar to for-loops, but are usually used for situations where the content to iterate through is indefinite.

//        int x = 0;
//        while(x < 10) {
//            System.out.println("Loop number: " + x);
//            x++;
//        }


        // 2. do-while Loop
        // is used to execute a block of statements continuously until the given condition is true

//        int y = 10;
//        do{
//            System.out.println("Countdown: " + y);
//            y--;
//        } while (y > 10);


        // 3. for Loop
        /* Syntax:
            for(initialValue; condition; iteration) {
                    // code block
            }
        */


//        for(int i = 0; i < 10; i++) {
            // int i = 0; -> initial value
            // i < 10 -> limiting expression/condition
            // o++ -> increment/decrement
//            System.out.println("Current count: " + i);
//        }


        // for Loop with Array/s
//        int[] intArray = {100, 200, 300, 400, 500};
//
//        for(int i = 0; i < intArray.length; i++) {
//            System.out.println(intArray[i]);
//        }


        // for-each Loop with Array
        /* Syntax:
                for(dataType itemName : arrayName) {
                    // code block
                }
         */
//        String[] nameArray = {"John", "Paul", "George", "Ringo"};
//        for(String name : nameArray) {
//            System.out.println(name);
//        }


        // Nested Loops with Multidimensional Arrays
        String[][] classroom = new String[3][3];
        //First row
        classroom[0][0] = "Athos";
        classroom[0][1] = "Porthos";
        classroom[0][2] = "Aramis";
        //Second row
        classroom[1][0] = "Brandon";
        classroom[1][1] = "JunJun";
        classroom[1][2] = "Jobert";
        //Third row
        classroom[2][0] = "Mickey";
        classroom[2][1] = "Donald";
        classroom[2][2] = "Goofy";

//        System.out.println(classroom.length);
//        System.out.println(classroom[0].length);
//        System.out.println(classroom[1].length);
//        System.out.println(classroom[2].length);

        // for Loop
        // outer loop
//        for(int row = 0; row < 3; row++) {
//            // inner loop
//            for(int col = 0; col < 3; col++) {
//                System.out.println("classroom[" + row + "][" + col + "] = " + classroom[row][col]);
//            }
//        }


        // forEach Loop with Multidimensional Array
        // accessing each row
        for(String[] row : classroom) {
            // accessing each column (actual value/element)
            for(String column : row) {
                System.out.println(column);
            }
        }


        // Example forEach Loop with ArrayList
        /* Syntax:
                arraylistName.forEach(Consumer<E> -> // code block);

                "->" This is called the lambda operator which is used to separate the parameter and implementation/expression.
        */
        ArrayList<Integer> numbers = new ArrayList<>();
        numbers.add(5);
        numbers.add(10);
        numbers.add(15);
        numbers.add(20);
        numbers.add(25);
        numbers.add(30);
//        System.out.println("ArrayList: " + numbers);
//        numbers.forEach(num -> System.out.println("ArrayList: " + num));


        // forEach with Hashmaps
        /* Syntax:
                hashMapNames.forEach((key, value) -> // code block);
        */
        HashMap<String, Integer> grades = new HashMap<String, Integer>() {
            {
                put("English", 90);
                put("Math", 95);
                put("Science", 97);
                put("History", 94);
            }
        };

//        grades.forEach((subject, grade) -> System.out.println(subject + " : " + grade + "\n"));


    }
}